@extends('layouts.master')

@section('content')
 <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">
        <h2 class="mb-4">Daftar pembayaran pelajar</h2>

        <div class="container">
  <form>

    <div class="row">
      <div class="col-md-5">
        <div class="form-group">
          <label for="first">Nama pelajar</label>
          <input type="text" class="form-control" placeholder="" id="input_namapelajar">
        </div>
      </div>
      <!--  col-md-6   -->
        <div class="col-md-2">
            <div class="form-group">
              <label for="Gender" class="select">Bin/Binti</label>
              <select class="form-control" id="selectinput_bin">
                  <option>Bin</option>
                  <option>Binti</option>
                </select>
            </div>
        </div>

      <div class="col-md-5">
        <div class="form-group">
          <label for="last">Nama ayah</label>
          <input type="text" class="form-control" placeholder="" id="input_namaayah">
        </div>
      </div>
      <!--  col-md-6   -->



    </div>


    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="kelas">Kelas</label>
          <input type="text" class="form-control" placeholder="" id="input_kelas">
        </div>


      </div>
      <!--  col-md-6   -->

      <div class="col-md-6">

        <div class="form-group">
          <label for="phone">No telefon rumah</label>
          <input type="tel" class="form-control" id="input_notelefonrumah" placeholder="phone">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>
    <!--  row   -->


    <div class="row">
      <div class="col-md-6">

        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" id="email" placeholder="email">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-6">
        <div class="form-group">
          <label for="url">Your Website <small>Please include http://</small></label>
          <input type="url" class="form-control" id="url" placeholder="url">
        </div>

      </div>
      <!--  col-md-6   -->
    </div>
    <!--  row   -->


    <label for="contact-preference">When is the best time of day to reach you?</label>
    <div class="radio">
      <label>
        <input type="radio" name="contact-preference" id="contact-preference" value="am" checked>Morning
      </label>
    </div>
    <div class="radio">
      <label>
        <input type="radio" name="contact-preference" id="contact-preference" value="pm" checked>Evening
      </label>
    </div>

    <label for="newsletter">Would you like to recieve our email newsletter?</label>
    <div class="checkbox">

      <label>
        <input type="checkbox" value="Sure!" id="newsletter"> Sure!
      </label>
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

      </div>

@endsection
