@extends('layouts.master')

@section('content')

<div id="content" class="p-4 p-md-5">
        <h2 class="mb-4">Senarai pelajar</h2>
<div class="container mt-2">
<div class="bs-example container" data-example-id="striped-table">
  <table class="table table-striped table-bordered table-hover">

    <thead>
      <tr>
        <th>#</th>
        <th>Nama pelajar</th>
        <th>Kelas</th>
        <th>No tel. rumah</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    @foreach($students as $student)
      <tr>
      	<td>{{$student->id}}</td>
        <td>{{$student->namapelajar}} {{ $student->bin }} {{ $student->namaayah}}</td>
        <td>{{$student->kelas}}</td>
        <td>{{$student->notelefonrumah}}</td>
        {{-- <td><a class="btn btn-sm btn-primary" name="" value="edit" href="/">Edit</a></td> --}}
        <td>
            {{-- <form action="{{ route('students.destroy',$student->id) }}" method="POST"> --}}

                <a class="btn btn-sm btn-info" href="{{ route('students.show',$student->id) }}"><i class="fas fa-edit"></i></a>

                {{-- <a class="btn btn-sm btn-primary" href="{{ route('students.edit',$student->id) }}">Edit</a>
				<a class='btn btn-info btn-xs' href="{{ route('students.show',$student->id) }}"><span class="glyphicon glyphicon-edit"></span></a>
                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-sm btn-danger">Delete</button> --}}
            {{-- </form> --}}
        </td>
      </tr>
    @endforeach

    </tbody>
  </table>
</div>
</div>
</div>
@endsection