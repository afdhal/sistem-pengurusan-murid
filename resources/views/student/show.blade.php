@extends('layouts.master')

@section('content')

<div id="content" class="p-4 p-md-5">
        <h2 class="mb-4">Maklumat pelajar</h2>
<div class="container mt-2">
<div class="container">
  <div class="row">
    <div class="col-7 col-sm-7 col-md-7 col-lg-7">
      <div class="card">
      	<div class="col-5 col-sm-5 col-md-5 col-lg-5">
	        <img class="card-img" src="https://png.pngtree.com/png-clipart/20190618/original/pngtree-school-student-illustration-student-pencil-male-student-hand-drawn-illustration-cartoon-png-image_3932877.jpg" alt="Vans">
	        <div class="card-img-overlay d-flex justify-content-end">
	          <a href="#" class="card-link text-danger like">
	            <i class="fas fa-heart"></i>
	          </a>
	        </div>
    	</div>
        <div class="card-body">
          <h4 class="card-title">{{$student[0]->namapelajar}}</h4>
          <h6 class="card-subtitle mb-2 text-muted">Kelas: {{$student[0]->kelas}}</h6>
          <h6 class="card-subtitle mb-2 text-muted">No tel rumah: {{$student[0]->notelefonrumah}}</h6>
          <p class="card-text">
            The Vans All-Weather MTE Collection features footwear and apparel designed to withstand the elements whilst still looking cool.             </p>


        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection