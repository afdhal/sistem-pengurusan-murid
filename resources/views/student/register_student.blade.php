@extends('layouts.master')

@section('content')
 <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">
        <h2 class="mb-4">Daftar pelajar</h2>

        <div class="container">
  <form action="/add_student_form" method="POST">
    @csrf

    <div class="row">
      <div class="col-md-5">
        <div class="form-group">
          <label for="first">Nama pelajar</label>
          <input type="text" class="form-control" placeholder="" id="input_namapelajar" name="namapelajar">
        </div>
      </div>
      <!--  col-md-6   -->
        <div class="col-md-2">
            <div class="form-group">
              <label for="Gender" class="select">Bin/Binti</label>
              <select class="form-control" id="selectinput_bin" name="bin">
                  <option value="bin">bin</option>
                  <option value="binti">binti</option>
                </select>
            </div>
        </div>

      <div class="col-md-5">
        <div class="form-group">
          <label for="last">Nama ayah</label>
          <input type="text" class="form-control" placeholder="" id="input_namaayah" name="namaayah">
        </div>
      </div>
      <!--  col-md-6   -->



    </div>


    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="kelas">Kelas</label>
          <input type="text" class="form-control" placeholder="" id="input_kelas" name="kelas">
        </div>


      </div>
      <!--  col-md-6   -->

      <div class="col-md-6">

        <div class="form-group">
          <label for="phone">No telefon rumah</label>
          <input type="tel" class="form-control" id="input_notelefonrumah" placeholder="" name="notelefonrumah">
        </div>
      </div>
      <!--  col-md-6   -->
    </div>
    <!--  row   -->


    <div class="row">
      <div class="col-md-6">

        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" id="email" placeholder="" name="email">
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-6">
        <div class="form-group">

        </div>

      </div>
      <!--  col-md-6   -->
    </div>
    <!--  row   -->

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

      </div>

@endsection
