<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/yuran', 'yuranController@index');
Route::get('/pay_yuran', 'yuranController@pay_yuran');

Route::resource('students', 'StudentController');
Route::get('/add_student_form', 'StudentController@open_form');
Route::post('/add_student_form', 'StudentController@save_form');
Route::get('/list_student', 'StudentController@list');
